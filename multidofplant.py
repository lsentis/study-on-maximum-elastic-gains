import math as mth
import numpy as np

class MultiDOFPlant:
	def __init__(self, inc_t, grav):
		self.inc_t = inc_t
		self.inertia = [1, 0.224]
		self.mass = [11.17, 6.82]
		self.center_of_mass = [0.3, 0.18]
		self.length = [0.35, 0.294]
		self.grav = grav
		self.Q = np.zeros((2,1))
		self.Q[1] = np.pi/5. # starting away from singularity
		self.dQ = np.zeros((2,1))
		self.ddQ = np.zeros((2,1))
		self.data = []

	def update(self, time, Torque_sensor):
		m1 = self.mass[0]
		m2 = self.mass[1]
		L1 = self.center_of_mass[0]
		L2 = self.center_of_mass[1]
		I1 = self.inertia[0]
		I2 = self.inertia[1]
		l1 = self.length[0]
		l2 = self.length[1]
		q1 = self.Q[0]
		q2 = self.Q[1]
		dq1 = self.dQ[0]
		dq2 = self.dQ[1]

		# kinematics
		xx = l1*mth.cos(q1)+l2*mth.cos(q1+q2)
		zz = l1*mth.sin(q1)+l2*mth.sin(q1+q2)
		self.P = np.mat([[xx],[zz]])

		J11 = -l1*mth.sin(q1)-l2*mth.sin(q1+q2)
		J12 = -l2*mth.sin(q1+q2)
		J21 = l1*mth.cos(q1)+l2*mth.cos(q1+q2)
		J22 = l2*mth.cos(q1+q2)
		self.J = np.mat([[J11, J12],[J21, J22]])

		self.dP = self.J * self.dQ

		# dynamics

		A11 = (m1*L1**2 + m2*l1**2 + m2*L2**2 + 2*m2*l1*L2*mth.cos(q2) 
			+ I1 + I2)
		A12 = m2*L2**2 + m2*l1*L2*mth.cos(q2) + I2	
		A21 = A12
		A22 = m2*L2**2 + I2
		self.A = np.mat([[A11, A12],[A21, A22]])

		self.Lambda = (self.J * self.A.I * self.J.T).I

		G1 = self.grav * ( m1*L1*mth.cos(q1) 
			 + m2*l1*mth.cos(q1) + m2*L2*mth.cos(q1+q2) )
		G2 = self.grav * m2*L2*mth.cos(q1+q2)
		self.G = np.mat([[G1],[G2]])

		self.GCart = (self.A.I * self.J.T * self.Lambda ).T * self.G

		B1 = -2*L1*l2*m2*mth.sin(q2) * dq1 * dq2 - L1*l2*m2*mth.sin(q2) * dq2**2
		B2 = L1*l2*m2*mth.sin(q2)*dq1**2
		self.B = np.mat([B1,B2])

		self.ddQ = self.A.I * ( Torque_sensor - self.B - self.G)
		self.dQ += self.ddQ * self.inc_t
		self.Q += self.dQ * self.inc_t + 0.5 * self.ddQ * self.inc_t * self.inc_t

		self.data.append([time, self.Q[0][0], self.Q[1][0], 
			self.P[0][0], self.P[1][0]])
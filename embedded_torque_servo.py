from high_level_position_servo_multi import *

class EmbeddedTorqueServo:
	def __init__(self, kp, kd, kd_damping_is_fixed = False
		, kd_damping_fixed_value = 0.):
		self.kp = kp
		self.kd = kd
		self.Torque_motor = 0
		self.data = []
		self.kd_damping_is_fixed = kd_damping_is_fixed
		self.kd_damping_fixed_value = kd_damping_fixed_value

	def update(self, time, Torque_sensor, dTorque_sensor, Torque_des, dq, kde = -1):
		# self.Torque_motor = (- self.kp * (Torque_sensor - Torque_des ) 
		# 	- self.kd * dTorque_sensor + Torque_des ) 

		if self.kd_damping_is_fixed:
			# self.Torque_motor = ( -self.kp * (Torque_sensor - Torque_des 
			# 	+ self.kd_damping_fixed_value * dq ) - self.kd * dTorque_sensor 
			# + Torque_des - self.kd_damping_fixed_value * dq) 
			# self.Torque_motor = (- self.kp * (Torque_sensor - Torque_des ) 
			# 	- self.kd * dTorque_sensor + Torque_des ) 
			self.Torque_motor = (- self.kp * (Torque_sensor - Torque_des 
				+ self.kd_damping_fixed_value * dq ) 
				- self.kd * dTorque_sensor + Torque_des 
				- self.kd_damping_fixed_value * dq) 
		else:
			self.Torque_motor = (- self.kp * (Torque_sensor - Torque_des 
				+ kde * dq ) - self.kd * dTorque_sensor + Torque_des - kde * dq) 

		self.data.append([time, self.Torque_motor] )
import math as mth
from robot_plant_multidof import *
from high_level_position_servo_multi import *
from compound_task import *

class HighLevelPositionServoMulti:
	def __init__(self, robot_plant_multidof, compound_task, embedded_servos
		, grav, Kp, Kd, debug = False):
		self.rpm = robot_plant_multidof
		self.ct = compound_task
		self.etsm = embedded_servos
		self.Kp = Kp
		self.Kd = Kd
		self.Kde = np.zeros((2,2))
		self.grav = grav
		self.debug = debug
		self.Torque_des = np.zeros((2,1))
		self.Torque_des_hist = []
		self.data = []

	def updateCartxJPos(self, time, x_des, dx_des, ddx_des, Q_des, dQ_des, ddQ_des):

		# gain weights
		w1 = 1
		w2 = 1

		A = self.rpm.mdpl.A
		J1 = self.ct.Jx
		L1 = self.ct.Lambdax
		pseudoJ1 = J1.T * np.linalg.pinv(J1 * J1.T)
		Q1 = w1 * J1.T * L1 * J1
		J2 = self.ct.S2
		L2 = self.ct.A2
		pseudoJ2 = J2.T * np.linalg.pinv(J2 * J2.T)
		Q2 = w2 * J2.T * L2 * J2
		pseudoQ = np.linalg.pinv(Q1 + Q2)
		self.kd1 = w1 * ( J1 * pseudoQ * self.etsm.Kd_damping * pseudoJ1 )
		self.Kd2 = w2 * ( J2 * pseudoQ * self.etsm.Kd_damping * pseudoJ2 )

		kp_nom_x = 100
		kd_nom_x = 2 * kp_nom_x**0.5

		kp_nom_q = 100
		kd_nom_q = 2 * kp_nom_q**0.5

		if ( self.kd1.item(0,0) / 2. ) ** 2 < kp_nom_x:
			kpx = ( self.kd1.item(0,0) / 2. ) ** 2
		else: # choose the nominal value
			kpx = kp_nom_x

		if ( self.Kd2.item(0,0) / 2. ) ** 2 < kp_nom_q:
			kpq1 = ( self.Kd2.item(0,0) / 2. ) ** 2
		else: # choose the nominal value
			kpq1 = kp_nom_q

		kpq_min = kpq1

		if ( self.Kd2.item(1,1) / 2. ) ** 2 < kp_nom_q:
			kpq2 = ( self.Kd2.item(1,1) / 2. ) ** 2
		else: # choose the nominal value
			kpq2 = kp_nom_q

		if kpq2 < kpq_min:
			kpq_min = kpq2

		self.kpx_eff = kpx
		self.kdx_eff = 2. * kpx ** 0.5

		self.Kpq_eff = np.mat([[kpq_min, 0],[0, kpq_min]])
		self.Kdq_eff = 2 * np.mat([[self.Kpq_eff.item(0,0) ** 0.5, 0], 
			[0, self.Kpq_eff.item(1,1) ** 0.5]])

		self.Kde = ( w1 * J1.T * L1 * self.kdx_eff * J1 
			+ w2 * J2.T * L2 * self.Kdq_eff * J2 )

		# self.Kde = ( w1 * J1.T * L1 * kd_nom_x * J1 
		# 	+ w2 * J2.T * L2 * kd_nom_q * J2 )

		if self.debug:
			print 'kpx_eff\n'
			print self.kpx_eff
			print '\n'

			print 'Kpq_eff\n' 
			print self.Kpq_eff
			print '\n'

			print 'Kde_eff\n'
			print self.Kde
			print '\n'

		# modulated high level gains
		self.U1 = ( ddx_des -self.kpx_eff * (self.ct.P.item(0) - x_des) 
			- self.kdx_eff * ( - dx_des ) )
			# - self.kdx_eff * ( self.ct.dx - dx_des ) )

		self.U2 = (ddQ_des -self.Kpq_eff * (self.rpm.mdpl.Q - Q_des) 
			- self.Kdq_eff * (- dQ_des) )
			# - self.Kdq_eff * (self.rpm.mdpl.dQ - dQ_des) )

		# fixed high level gains
		# self.U1 = ( ddx_des -kp_nom_x * (self.ct.P.item(0) - x_des) 
		# 	- kd_nom_x * ( self.ct.dx - dx_des ) )

		# self.U2 = (ddQ_des -kp_nom_q * (self.rpm.mdpl.Q - Q_des) 
		# 	- kd_nom_q * (self.rpm.mdpl.dQ - dQ_des) )

		self.Torque_des = ( 
			self.ct.Jx.T * ( self.ct.Lambdax * self.U1 + self.ct.Gx ) + 
			self.ct.S2.T * ( self.ct.A2 * self.U2 + self.ct.G2 ) 
			)

		self.Torque_des_hist.append([self.Torque_des.item(0), 
			self.Torque_des.item(1)])

		self.data.append([time, self.Torque_des.item(0), 
			self.Torque_des.item(1), x_des])

	def updateCartxz(self, time, P_des, dP_des, ddP_des):
		# gain translation

		if self.etsm.damping_is_fixed:
			A = self.rpm.mdpl.A
			J = self.rpm.mdpl.J
			L = self.rpm.mdpl.Lambda
			barJ = A.I * J.T * L
			Q = J.T * L * J
			# barQ = np.linalg.pinv(Q)
			self.Kd = ( J * Q.I * self.etsm.Kd_damping * barJ )

			if ( self.Kd.item(0,0) / 2. ) ** 2 < self.Kp.item(0,0):
				kp1 = ( self.Kd.item(0,0) / 2. ) ** 2
			else: # choose the nominal value
				kp1 = self.Kp.item(0,0)

			if ( self.Kd.item(1,1) / 2. ) ** 2 < self.Kp.item(1,1):
				kp2 = ( self.Kd.item(1,1) / 2. ) ** 2
			else: # choose the nominal value
				kp2 = self.Kp.item(1,1)

			self.Kp_eff = np.mat([[kp1, 0],[0, kp2]])
			self.Kd_eff = 2 * np.mat([[self.Kp_eff.item(0,0) ** 0.5, 0]
				, [0, self.Kp_eff.item(1,1) ** 0.5]])

			# # diagonalize Kd
			# kdo1 = self.Kd.item(0,1)
			# kdo2 = self.Kd.item(1,0)
			# self.Kd_off_diag = np.mat([[0, kdo1], [kdo2, 0]])
			# self.Kd_eff = self.Kd - self.Kd_off_diag

			if self.debug:
				print 'Kp high_level_position_servo_multi'
				print self.Kp_eff

			self.U = ( ddP_des -self.Kp_eff * (self.rpm.mdpl.P - P_des) 
				- self.Kd_eff * ( self.rpm.mdpl.dP - dP_des ) )

			# self.U = ( ddP_des -self.Kp_eff * (self.rpm.mdpl.P - P_des) 
			# 	+ self.Kd_off_diag * self.rpm.mdpl.dP 
			# 	+ (self.Kd - self.Kd_off_diag ) * dP_des )

		else: 
			self.Kde = ( self.rpm.mdpl.J.T * self.rpm.mdpl.Lambda 
			* self.Kd * self.rpm.mdpl.J )
			
			if self.debug:
				print 'Kde'
				print self.Kde

			# only the desired velocity. the other part is on the embedded level
			self.U = (ddP_des -self.Kp * (self.rpm.mdpl.P - P_des) 
				+ self.Kd * dP_des)

			# current and desired velocity are used here at the high level
			# self.U = (ddP_des -self.Kp * (self.rpm.mdpl.P - P_des) 
			# 	- self.Kd * (self.rpm.mdpl.dP - dP_des) )

	
		self.Torque_des = ( self.rpm.mdpl.J.T * 
			( self.rpm.mdpl.Lambda * self.U + self.rpm.mdpl.GCart ) )

		self.Torque_des_hist.append([self.Torque_des.item(0), 
			self.Torque_des.item(1)])

		self.data.append([time, self.Torque_des.item(0), 
			self.Torque_des.item(1), P_des.item(0), P_des.item(1)])

	def updateJpos (self, time, Q_des, dQ_des, ddQ_des):
		self.U = (ddQ_des -self.Kp * (self.rpm.mdpl.Q - Q_des) 
			- self.Kd * (self.rpm.mdpl.dQ - dQ_des) )
		self.Torque_des = self.rpm.mdpl.A * self.U + self.rpm.mdpl.G






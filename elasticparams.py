# this class defines elastic parameters

class ElasticParams: # same for all actuators
	rotor_weight = 0.1
	rotor_radius = 0.03
	gear_ratio = 160
	stiffness = 4000 #Nm/rad
	gear_friction = 60 #Nm/rad/s

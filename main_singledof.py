#!/usr/bin/python

from robot_plant_singledof import *
from embedded_torque_servo import *
from embedded_position_servo import *
from embedded_velocity_servo import *
from high_level_position_servo import *
from inverse_dynamics import *
import matplotlib.pyplot as plt
import math as mth

# objects
inc_t = 0.001
grav_real = 10.
grav_estimated = 0.8 * grav_real
rps = RobotPlantSingle(inc_t, grav_real)
kp_torque = 5.
kd_torque = 0.2
ets = EmbeddedTorqueServo(kp_torque, kd_torque)
kp_position_embedded = 4000.
kd_position_embedded = 2.*mth.sqrt( kp_position_embedded) 
eps = EmbeddedPositionServo(kp_position_embedded, kd_position_embedded)
kd_velocity_embedded = 120
evs = EmbeddedVelocityServo(kd_velocity_embedded)
kp_position_high = 55. # range of acceptable gains is relatively low
kd_position_high = 2.*mth.sqrt( kp_position_high)
inertia_estimated = 0.8 * rps.lpa.inertia
com_estimated = 0.8 * rps.lpa.center_of_mass
mass_estimated = 0.7 * rps.lpa.mass
hlps = HighLevelPositionServo(kp_position_high, kd_position_high, inertia_estimated, 
	mass_estimated, com_estimated, grav_estimated)
idd = InverseDynamics(inc_t, inertia_estimated, 
	mass_estimated, com_estimated, grav_estimated)

# inits
# Torque_des = 30.
# q_des = 0.5
# dq_des = 0

# options
# op_strategy = 'torque_to_vel'
op_strategy = 'pure_torque'
# op_strategy = 'torque_to_pos'

# user parameter - position frequency
position_servo = 10;#ms [1-r]. needs to be smaller than delay
position_count = 0;

# delay
delay = 20 # x times the embedded servo loop
count_delay = 0

# embedded loop
tt = 0
while tt < 10:

	# trajectories
	ff = 0.3 # it has problems to track at higher frequencies
	ampl = 1.2
	q_des = ampl*mth.sin(2*mth.pi*ff*tt)
	dq_des = ampl*2*mth.pi*ff*mth.cos(2*mth.pi*ff*tt)
	ddq_des = -ampl*2*mth.pi*ff*2*mth.pi*ff*mth.sin(2*mth.pi*ff*tt)

	# copy to compute torque sensor derivative
	Torque_sensor_before = rps.epl.Torque_sensor

	# update elastic plant
	rps.epl.update(tt, rps.sdpl.q, ets.Torque_motor)

	# compute torque sensor derivative
	dTorque_sensor = (rps.epl.Torque_sensor-Torque_sensor_before)/inc_t

	# update multidof plant
	rps.sdpl.update( tt, rps.epl.Torque_sensor )
	
	# position loop (slower than torque loop)
	if position_count >= position_servo:
		# high level position servo
		hlps.update(tt, rps.sdpl.q, rps.sdpl.dq, q_des, dq_des, ddq_des)
		position_count = 0

	position_count += 1


	# time delay 1ms per loop
	if count_delay < delay:
		Torque_des = 0
		count_delay += 1
	else:
		Torque_des = hlps.Torque_des_hist[-delay/position_servo][0]

	if op_strategy == 'torque_to_vel' or op_strategy == 'torque_to_pos':

		# inverse dynamics with integration
		idd.solve_Runge_Kutta(tt, rps.sdpl.q, rps.sdpl.dq, Torque_des)

		# embedded position servo
		# eps.update(tt, rps.sdpl.q, rps.sdpl.dq, q_des)
		eps.update(tt, rps.sdpl.q, rps.sdpl.dq, idd.q_des, idd.dq_des, idd.ddq_des)

		# embedded velocity servo
		evs.update(tt, rps.sdpl.dq, idd.dq_des, idd.ddq_des)

		# embedded torque servo
		ets.update( tt, rps.epl.Torque_sensor, dTorque_sensor, eps.Torque_des)
	
	elif op_strategy == 'pure_torque':

		ets.update( tt, rps.epl.Torque_sensor, dTorque_sensor, Torque_des)

	tt += inc_t

print 'done'

data1 = np.array( rps.epl.data )
data2 = np.array( rps.sdpl.data )
data3 = np.array( ets.data )
data5 = np.array( hlps.data )

if op_strategy == 'torque_to_pos':
	data4 = np.array( eps.data )
	data6 = np.array( idd.data )
elif op_strategy == 'torque_to_vel':
	data4 = np.array( evs.data )
	data6 = np.array( idd.data )

# plotting
legend_size = 8

plt.clf()
plt.subplot(2,2,1)
plt.plot( data1[:,0], data1[:,1], label = 'Theta' )
plt.plot( data2[:,0], data2[:,1], label = 'q' )	
plt.plot( data5[:,0], data5[:,2], label = 'q_des')
if op_strategy == 'torque_to_pos' or op_strategy == 'torque_to_vel':
	plt.plot( data6[:,0], data6[:,1], label = 'q des inv dyn')
plt.xlabel('t[s]')
plt.ylabel('Angle [rad]')
plt.title('Joint Position')
plt.legend(prop={'size':legend_size})

plt.subplot(2,2,2)
plt.plot( data1[:,0], data1[:,3], label = 'Sensor' )
plt.plot( data5[:,0], data5[:,1], label = 'Des High' )
if op_strategy == 'torque_to_pos' or op_strategy == 'torque_to_vel':
	plt.plot( data4[:,0], data4[:,1], label = 'Des Emb' )
plt.xlabel('t[s]')
plt.ylabel('Torque [Nm]')
plt.title('Torque Sensor')
plt.legend(prop={'size':legend_size})

plt.subplot(2,2,3)
plt.plot( data1[:,0], data1[:,2], label = 'Sensor' )
plt.plot( data2[:,0], data2[:,2], label = 'Desired' )
plt.xlabel('t[s]')
plt.ylabel('Angular Velocity [rad/s]')
plt.title('Joint Velocity')
plt.legend(prop={'size':legend_size})

plt.subplot(2,2,4)
plt.plot( data3[:,0], data3[:,1], label = 'Motor' )
plt.plot( data5[:,0], data5[:,1], label = 'Des High' )
if op_strategy == 'torque_to_pos' or op_strategy == 'torque_to_vel':
	plt.plot( data4[:,0], data4[:,1], label = 'Des Emb' )
plt.xlabel('t[s]')
plt.ylabel('Torque [Nm]')
plt.title('Torque Motor')
plt.legend(prop={'size':legend_size})

# adjusting subplot
wadjust = 0.4
hadjust = 0.4
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure.png')
# plt.show()

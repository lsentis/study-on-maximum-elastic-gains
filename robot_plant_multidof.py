from elasticparams import *
from elasticplant import *
from multidofplant import *

class RobotPlantMulti:

	def __init__(self, inc_t, grav):
		self.epa = ElasticParams()
		self.epl1 = ElasticPlant(inc_t, self.epa.rotor_weight, self.epa.rotor_radius, 
			self.epa.gear_ratio, self.epa.gear_friction, self.epa.stiffness)
		self.epl2 = ElasticPlant(inc_t, self.epa.rotor_weight, self.epa.rotor_radius, 
			self.epa.gear_ratio, self.epa.gear_friction, self.epa.stiffness)
		self.mdpl = MultiDOFPlant(inc_t, grav)

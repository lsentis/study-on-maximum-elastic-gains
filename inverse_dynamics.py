import math as mth

class InverseDynamics:
	def __init__(self, inc_t, load_inertia, mass, center_of_mass, grav):
		self.inc_t = inc_t
		self.load_inertia = load_inertia
		self.mass = mass
		self.center_of_mass = center_of_mass
		self.grav = grav
		self.q_des = 0
		self.dq_des = 0
		self.ddq_des = 0
		self.data = []

	def solve_Runge_Kutta(self, time, q, dq, Torque_des):
		# iter 1
		q1 = q
		dq1 = dq
		ddq1 = self.accel_func(q1, dq1, Torque_des)
		
		# iter 2
		q2 = q + 0.5 * dq1 * self.inc_t
		dq2 = dq + 0.5 * ddq1 * self.inc_t
		ddq2 = self.accel_func(q2, dq2, Torque_des)

		# iter 3
		q3 = q + 0.5 * dq2 * self.inc_t
		dq3 = dq + 0.5 * ddq2 * self.inc_t
		ddq3 = self.accel_func(q3, dq3, Torque_des)

		# iter 4
		q4 = q + 0.5 * dq3 * self.inc_t
		dq4 = dq + 0.5 * ddq3 * self.inc_t
		ddq4 = self.accel_func(q4, dq4, Torque_des)

		self.q_des += (self.inc_t/6.0)*(dq1 + 2*dq2 + 2*dq3 + dq4)
		self.dq_des += (self.inc_t/6.0)*(ddq1 + 2*ddq2 + 2*ddq3 + ddq4)

		self.ddq_des = self.accel_func( q, dq, Torque_des)

		self.data.append([time, self.q_des, self.dq_des, self.ddq_des])

	def accel_func(self, q, dq, Torque_des):
		return 1./self.load_inertia * (
			Torque_des - self.mass * self.center_of_mass * self.grav * mth.sin( q ))

	def solve_Euler(self, time, q, dq, Torque_des):
		self.ddq_des = self.accel_func( q, dq, Torque_des)
		self.dq_des += self.ddq_des * self.inc_t
		self.q_des += (self.dq_des * self.inc_t 
			+ 0.5 * self.ddq_des * self.inc_t * self.inc_t)
		self.data.append([time, self.q_des, self.dq_des, self.ddq_des])




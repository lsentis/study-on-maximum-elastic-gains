#!/usr/bin/python

from robot_plant_multidof import *
from embedded_torque_servo import *
from embedded_torque_servo_multi import *
from high_level_position_servo_multi import *
from compound_task import *
import matplotlib.pyplot as plt
import math as mth

# objects
inc_t = 0.001
grav = 10.#10.
rpm = RobotPlantMulti(inc_t, grav)

# user choice
debug = True
Kde_is_fixed = True

if Kde_is_fixed:
	# fixed Kde
	etsm = EmbeddedTorqueServoMulti(True)
else:
	# non fixed Kde
	etsm = EmbeddedTorqueServoMulti(False)

# behavior
ct = CompoundTask(rpm.mdpl)

kp_high1 = 200 
kp_high2 = 200 
kd_high1 = 2.*mth.sqrt( kp_high1)
kd_high2 = 2.*mth.sqrt( kp_high2)
Kp_high = np.mat([[kp_high1, 0],[0, kp_high2]])
Kd_high = np.mat([[kd_high1, 0],[0, kd_high2]])
hlps = HighLevelPositionServoMulti(rpm, ct, etsm, grav, Kp_high, Kd_high, debug)

# inits
# Q_des = np.mat([[1.2],[0.4]])
Q_des = np.mat([[0.],[1.57]])
dQ_des = np.zeros((2,1))
ddQ_des = np.zeros((2,1))

P_des = np.mat([[0.13],[0.13]])
dP_des = np.zeros((2,1))
ddP_des = np.zeros((2,1))

# delay
delay = 15#15 # minimum 1. x times the embedded servo loop
count_delay = 0

# embedded loop
tt = 0
while tt < 5:

	ff = 0.3 # it has problems to track at higher frequencies
	ampl = 0.2
	P_des[0] = ampl*mth.sin(2*mth.pi*ff*tt)
	dP_des[0] = ampl*2*mth.pi*ff*mth.cos(2*mth.pi*ff*tt)
	ddP_des[0] = -ampl*2*mth.pi*ff*2*mth.pi*ff*mth.sin(2*mth.pi*ff*tt)

	# copy to compute torque sensor derivative
	Torque_sensor_before1 = rpm.epl1.Torque_sensor
	Torque_sensor_before2 = rpm.epl2.Torque_sensor

	# update elastic plant
	rpm.epl1.update(tt, rpm.mdpl.Q[0], etsm.ets1.Torque_motor)
	rpm.epl2.update(tt, rpm.mdpl.Q[1], etsm.ets2.Torque_motor)
	Torque_sensor1 = rpm.epl1.Torque_sensor
	Torque_sensor2 = rpm.epl2.Torque_sensor

	# compute torque sensor derivative
	dTorque_sensor1 = (Torque_sensor1-Torque_sensor_before1)/inc_t
	dTorque_sensor2 = (Torque_sensor2-Torque_sensor_before2)/inc_t

	# update multidof plant
	Torque_sensor = np.mat([Torque_sensor1,Torque_sensor2])
	rpm.mdpl.update(tt, Torque_sensor)

	# update compound task
	ct.update()

	# high level position servo joint pos
	# hlps.update(tt, Q_des, dQ_des, ddQ_des)
	# Torque_des1 = hlps.Torque_des.item(0)
	# Torque_des2 = hlps.Torque_des.item(1)

	# high level position servo Cart pos
	# hlps.updateCartxz(tt, P_des, dP_des, ddP_des)
	hlps.updateCartxJPos(tt, P_des.item(0), dP_des.item(0), ddP_des.item(0),
		Q_des, dQ_des, ddQ_des)

	# time delay 1ms per loop
	if count_delay < delay:
		Torque_des1 = 0
		Torque_des2 = 0
		count_delay += 1
	else:
		Torque_des1 = hlps.Torque_des_hist[-1-delay][0]
		Torque_des2 = hlps.Torque_des_hist[-1-delay][1]

	etsm.ets1.update( tt, Torque_sensor1, dTorque_sensor1, 
		Torque_des1, rpm.mdpl.dQ.item(0), hlps.Kde.item(0,0))
	etsm.ets2.update( tt, Torque_sensor2, dTorque_sensor2, 
		Torque_des2, rpm.mdpl.dQ.item(1), hlps.Kde.item(1,1))

	tt += inc_t

print 'done'

data1 = np.array( rpm.mdpl.data )
data2 = np.array( hlps.data )

# plotting
legend_size = 8

plt.clf()
plt.subplot(2,2,1)
plt.plot( data1[:,0], data1[:,1], label = 'q1' )
plt.xlabel('t[s]')
plt.ylabel('Angle [rad]')
plt.title('Joint Position 1')
# plt.axis([0,8,-0.1, 1.7])
plt.legend(prop={'size':legend_size})

plt.subplot(2,2,3)
plt.plot( data1[:,0], data1[:,2], label = 'q2' )
plt.xlabel('t[s]')
plt.ylabel('Angle [rad]')
plt.title('Joint Position 2')
plt.legend(prop={'size':legend_size})

plt.subplot(2,2,2)
plt.plot( data1[:,0], data1[:,3], label = 'x' )
plt.plot( data2[:,0], data2[:,3], label = 'x des' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Cart Position 1')
# plt.axis([0,8,-0.1, 1.7])
plt.legend(prop={'size':legend_size})

plt.subplot(2,2,4)
plt.plot( data1[:,0], data1[:,4], label = 'z' )
# plt.plot( data2[:,0], data2[:,4], label = 'z des' )
plt.xlabel('t[s]')
plt.ylabel('[m]')
plt.title('Cart Position 2')
plt.legend(prop={'size':legend_size})

# adjusting subplot
wadjust = 0.4
hadjust = 0.4
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure.png')

plt.clf()
plt.subplot(2,1,1)
plt.plot( data1[:,3], data1[:,4])
plt.xlabel('x [m]')
plt.ylabel('z [m]')
plt.title('Cart Position')

plt.subplot(2,1,2)
error1 = data1[:,1]-Q_des.item(0) * np.ones( np.shape( data1[:,1] ) )
error2 = data1[:,2]-Q_des.item(1) * np.ones( np.shape( data1[:,2] ) )
print error2
plt.plot( data1[:,0], ( error1**2 + error2**2 ) ** 0.5 )
plt.xlabel('t [s]')
plt.ylabel('Error [rad]')
plt.title('JPos Error')

# adjusting subplot
wadjust = 0.4
hadjust = 0.4
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure2.png')


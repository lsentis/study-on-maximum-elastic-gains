class EmbeddedPositionServo:
	def __init__(self, kp, kd):
		self.kp = kp
		self.kd = kd
		self.Torque_des = 0
		self.data = []

	def update(self, time, q, dq, q_des, dq_des, ddq_des):
		# position and velocity
		self.Torque_des = ddq_des -self.kp * (q-q_des) - self.kd * (dq - dq_des)
		self.data.append([time, self.Torque_des, q_des, dq_des, ddq_des])


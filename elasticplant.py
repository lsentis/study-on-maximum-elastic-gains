import math as mth

class ElasticPlant:

	def __init__(self, inc_t, rotor_weight, rotor_radius, 
		gear_ratio, gear_friction, stiffness):
		self.inc_t = inc_t
		self.JJ = (rotor_weight * rotor_radius * rotor_radius / 2.0 
			* gear_ratio * gear_ratio)
		self.BB = gear_friction
		self.KK = stiffness
		self.Theta = 0.
		self.dTheta = 0.
		self.ddTheta = 0.
		self.Torque_sensor = 0
		self.data = []

	def update(self, time, q_before, Torque_motor):
		# Euler integration based on rotor-gear-spring system
		self.ddTheta = 1./self.JJ * ( Torque_motor - self.BB * self.dTheta 
			- self.KK* ( self.Theta - q_before ) )
		self.dTheta += self.ddTheta * self.inc_t
		self.Theta += (self.dTheta * self.inc_t 
			+ 0.5 * self.ddTheta * self.inc_t * self.inc_t)
		self.Torque_sensor = self.KK * ( self.Theta - q_before )
		self.data.append([time, self.Theta, self.dTheta, self.Torque_sensor])

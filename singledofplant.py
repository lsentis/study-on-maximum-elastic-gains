import math as mth
import numpy as np

class SingleDOFPlant:
	def __init__(self, inc_t, load_inertia, mass, center_of_mass, grav):
		self.inc_t = inc_t
		self.load_inertia = load_inertia
		self.mass = mass
		self.center_of_mass = center_of_mass
		self.grav = grav
		self.q = 0
		self.dq = 0
		self.ddq = 0
		self.data = []

	def update(self, time, Torque_sensor):
		self.ddq = 1./self.load_inertia * ( Torque_sensor 
			- self.mass * self.center_of_mass * self.grav * mth.sin(self.q) )
		self.dq += self.ddq * self.inc_t
		self.q += self.dq * self.inc_t + 0.5 * self.ddq * self.inc_t * self.inc_t
		self.data.append( [time, self.q, self.dq] )


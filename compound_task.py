import math as mth
import numpy as np

class CompoundTask:
	def __init__(self, multi_dof_plant):
		self.mdpl = multi_dof_plant

	def update(self):
		m1 = self.mdpl.mass[0]
		m2 = self.mdpl.mass[1]
		L1 = self.mdpl.center_of_mass[0]
		L2 = self.mdpl.center_of_mass[1]
		I1 = self.mdpl.inertia[0]
		I2 = self.mdpl.inertia[1]
		l1 = self.mdpl.length[0]
		l2 = self.mdpl.length[1]

		q1 = self.mdpl.Q[0]
		q2 = self.mdpl.Q[1]
		dq1 = self.mdpl.dQ[0]
		dq2 = self.mdpl.dQ[1]		

		xx = l1*mth.cos(q1)+l2*mth.cos(q1+q2)
		zz = l1*mth.sin(q1)+l2*mth.sin(q1+q2)
		self.P = np.mat([[xx],[zz]])

		J11 = -l1*mth.sin(q1)-l2*mth.sin(q1+q2)
		J12 = -l2*mth.sin(q1+q2)
		J21 = l1*mth.cos(q1)+l2*mth.cos(q1+q2)
		J22 = l2*mth.cos(q1+q2)
		self.Jxz = np.mat([[J11, J12],[J21, J22]])

		self.dPxz = self.Jxz * self.mdpl.dQ
		self.Lambdaxz = (self.Jxz * self.mdpl.A.I * self.Jxz.T).I
		self.Gxz = (self.mdpl.A.I * self.Jxz.T * self.Lambdaxz ).T * self.mdpl.G

		# task 1
		self.Jx = np.mat([[J11, J12]])
		self.dx = self.Jx * self.mdpl.dQ
		self.Lambdax = (self.Jx * self.mdpl.A.I * self.Jx.T).I
		self.barJx = self.mdpl.A.I * self.Jx.T * self.Lambdax
		self.Gx = self.barJx.T * self.mdpl.G

		# null space 1
		Id2 = np.identity(2)
		self.Nullx = Id2 - self.barJx * self.Jx

		# phi 1
		self.phi1 = self.Nullx * self.mdpl.A.I * self.Nullx.T

		# task 2
		self.S2 = np.identity(2) * self.Nullx
		self.phi2 = (self.S2 * self.phi1 *self.S2.T)
		self.A2 = np.linalg.pinv(self.phi2)
		self.barS2 = self.phi1 * self.S2.T * self.A2
		self.G2 = self.barS2.T * self.mdpl.G

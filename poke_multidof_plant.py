#!/usr/bin/python

from robot_plant_multidof import *
from embedded_torque_servo import *
from high_level_position_servo_multi import *
import matplotlib.pyplot as plt
import math as mth

# objects
inc_t = 0.001
grav = 10.
rpm = RobotPlantMulti(inc_t, grav)
kp_torque = 5.
kd_torque = 0.2
ets1 = EmbeddedTorqueServo(kp_torque, kd_torque)
ets2 = EmbeddedTorqueServo(kp_torque, kd_torque)
kp_high1 = 55 
kp_high2 = 55 
kd_high1 = 2.*mth.sqrt( kp_high1)
kd_high2 = 2.*mth.sqrt( kp_high2)

Kp_high = np.mat([[kp_high1, 0],[0, kp_high2]])
Kd_high = np.mat([[kd_high1, 0],[0, kd_high2]])

hlps = HighLevelPositionServoMulti(rpm, Kp_high, Kd_high, grav)

# inits
Torque_mot1 = 0.
Torque_mot2 = 0.
# q_des = 0.5
# dq_des = 0

# embedded loop
tt = 0
while tt < 8:

	rpm.epl1.update(tt, rpm.mdpl.Q[0], Torque_mot1)
	rpm.epl2.update(tt, rpm.mdpl.Q[1], Torque_mot2)
	Torque_sensor1 = rpm.epl1.Torque_sensor
	Torque_sensor2 = rpm.epl2.Torque_sensor
	Torque_sensor = np.mat([Torque_sensor1,Torque_sensor2])
	rpm.mdpl.update(tt, Torque_sensor)
	tt += inc_t

print 'done'

data1 = np.array( rpm.mdpl.data )

# plotting
legend_size = 8

plt.clf()

plt.subplot(2,2,1)
plt.plot( data1[:,0], data1[:,1], label = 'q1' )
plt.xlabel('t[s]')
plt.ylabel('Angle [rad]')
plt.title('Joint Position 1')
plt.legend(prop={'size':legend_size})

plt.subplot(2,2,3)
plt.plot( data1[:,0], data1[:,2], label = 'q2' )
plt.xlabel('t[s]')
plt.ylabel('Angle [rad]')
plt.title('Joint Position 2')
plt.legend(prop={'size':legend_size})

# adjusting subplot
wadjust = 0.4
hadjust = 0.4
plt.subplots_adjust(wspace = wadjust)
plt.subplots_adjust(hspace = hadjust)
plt.savefig('figure.png')


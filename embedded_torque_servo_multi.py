from embedded_torque_servo import *
import numpy as np

class EmbeddedTorqueServoMulti:
	def __init__(self, damping_is_fixed):
		kp_torque = 25.
		kd_torque = 0.2
		self.damping_is_fixed = damping_is_fixed

		if damping_is_fixed:
			kd_damping1 = 40
			kd_damping2 = 40
			self.Kd_damping = np.mat([[kd_damping1, 0], [0, kd_damping2]])
			self.ets1 = EmbeddedTorqueServo(kp_torque, kd_torque
				, self.damping_is_fixed, kd_damping1)
			self.ets2 = EmbeddedTorqueServo(kp_torque, kd_torque
				, self.damping_is_fixed, kd_damping2)
		else:
			self.ets1 = EmbeddedTorqueServo(kp_torque, kd_torque)
			self.ets2 = EmbeddedTorqueServo(kp_torque, kd_torque)
#!/usr/bin/python

from robotmodel import *
import matplotlib.pyplot as plt

# inits
inc_t = 0.001
grav = 10

# robotmodel
rm = RobotModel(inc_t, grav)
# print rm.epa.rotor_weight

# poking
Torque_motor = 30 # the poke
Torque_sensor = 0

# poking loop
tt = 0
while tt < 5.:

	rm.epl.update(tt, rm.mdpl.q, Torque_motor)
	rm.mdpl.update( tt, rm.epl.Torque_sensor )
	tt += inc_t

print 'done'

data1 = np.array( rm.epl.data )
data2 = np.array( rm.mdpl.data )

# plotting
plt.clf()
plt.plot( data1[:,0], data1[:,1], label = 'Theta' )
plt.plot( data2[:,0], data2[:,1], label = 'q' )	
plt.xlabel('t[s]')
plt.ylabel('Angle [rad]')
plt.title('Joint Position')
plt.legend()
plt.savefig('figure.png')

# plotting
plt.clf()
plt.plot( data1[:,0], data1[:,3], label = 'Sensor' )
plt.xlabel('t[s]')
plt.ylabel('Torque [Nm]')
plt.title('Torque Sensor')
plt.savefig('figure2.png')

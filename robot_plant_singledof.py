from elasticparams import *
from linkparams import *
from elasticplant import *
from singledofplant import *

class RobotPlantSingle:

	def __init__(self, inc_t, grav):
		self.epa = ElasticParams()
		self.lpa = LinkParams()
		self.epl = ElasticPlant(inc_t, self.epa.rotor_weight, self.epa.rotor_radius, 
			self.epa.gear_ratio, self.epa.gear_friction, self.epa.stiffness)
		self.sdpl = SingleDOFPlant(inc_t, self.lpa.inertia, 
			self.lpa.mass, self.lpa.center_of_mass, grav)

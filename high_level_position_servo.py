import math as mth

class HighLevelPositionServo:
	def __init__(self, kp, kd, load_inertia, mass, center_of_mass, grav):
		self.kp = kp
		self.kd = kd
		self.load_inertia = load_inertia
		self.mass = mass
		self.center_of_mass = center_of_mass
		self.grav = grav
		self.Torque_des = 0
		self.Torque_des_hist = []
		self.data = []

	def update (self, time, q, dq, q_des, dq_des, ddq_des):
		self.u = ddq_des -self.kp * (q-q_des) - self.kd * (dq-dq_des)
		self.Torque_des = (self.load_inertia * self.u 
			+ self.mass * self.center_of_mass * self.grav * mth.sin( q ) )
		self.Torque_des_hist.append([self.Torque_des])
		self.data.append([time, self.Torque_des, q_des, dq_des, ddq_des])

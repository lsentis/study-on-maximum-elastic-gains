class EmbeddedVelocityServo:
	def __init__(self, kd):
		self.kd = kd
		self.Torque_des = 0
		self.data = []

	def update(self, time, dq, dq_des, ddq_des):
		# velocity only
		self.Torque_des = ddq_des  - self.kd * (dq - dq_des)
		self.data.append([time, self.Torque_des, dq_des, ddq_des])
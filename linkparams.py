# this file defines the static link parameters of a 3 dof planar fixed robot 
# it mimics the flexion joints of NASA Valkyrie's leg

class LinkParams:
	length = 0.4#(0.4, 0.4, 0.1)
	inertia = 5#(5, 0.7, 0.01)
	mass = 17#(15, 7, 0.5)
	center_of_mass = 0.43#(0.2,0.2,0.1)